# !/bin/bash

DATABASE_IP=$1

if [ ! "$DATABASE_IP" ]; then
	echo "Usage: $0 <DATABASE_IP>"
	exit -1
fi

sudo sed "s/host=.*/host=$DATABASE_IP/" /opt/trac/trac.ini
