#!/bin/bash

PID_FILE=/var/run/trac.pid
LOG_FILE=/var/log/trac.log

nohup tracd --port 5000 /opt/trac > $LOG_FILE 2>&1 &
echo $! > $PID_FILE
exit $?
